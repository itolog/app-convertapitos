import { createRouter, RouterProvider } from "@tanstack/react-router";
import { StrictMode, Suspense } from "react";
import ReactDOM from "react-dom/client";
import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import { snackbar } from "@/config";
import { SnackbarProvider } from "notistack";

import { CssBaseline } from "@mui/material";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import { persistor, store } from "@/store/store.ts";

import { routeTree } from "./routeTree.gen";
import i18n from "./translations";

import "@styles/main.scss";

const router = createRouter({
	routeTree,
});

declare module "@tanstack/react-router" {
	interface Register {
		router: typeof router;
	}
}

// Render the app
const rootElement = document.getElementById("root")!;
if (!rootElement.innerHTML) {
	const root = ReactDOM.createRoot(rootElement);
	root.render(
		<StrictMode>
			<CssBaseline />
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistor}>
					<Suspense fallback={<div>Loading...</div>}>
						<I18nextProvider i18n={i18n}>
							<SnackbarProvider {...snackbar}>
								<RouterProvider router={router} />
							</SnackbarProvider>
						</I18nextProvider>
					</Suspense>
				</PersistGate>
			</Provider>
		</StrictMode>,
	);
}
